﻿using Autofac.Registrars;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Linq;

namespace InstrumentIt
{
    public partial class PerfCounters : IFluentInterface  
    {
        // Global list of counter group references
        private static IDictionary<string, PerfCounters> categoryList = new Dictionary<string, PerfCounters>();

        private static object syncLock = new object();

        private bool        recreateCategory = false;
        private string      categoryName = string.Empty;
        private string      machineName = ".";
        private CounterCategory Counters = new CounterCategory();




        public string CategoryName
        {
            get
            {
                return this.categoryName;
            }
        }


        private PerfCounters()
        { }

        public static PerfCounters Initialize(bool recreateCategory = false)
        {
            var perfCounters = new PerfCounters(); 
            perfCounters.recreateCategory = recreateCategory;
            
            return perfCounters; 
        }

        public PerfCounters OnMachine(string machineName)
        {
            this.machineName= machineName;
            return this;
        }

        public PerfCounters WithCategory(string categoryName)
        {
            this.categoryName = categoryName;
            
            if ( !categoryList.ContainsKey(categoryName))
            {
                categoryList.Add (categoryName, this);
            }

            return this;
        }

        internal PerfCounters AndCounter(string counterName, PerformanceCounterType counterType, string helpText = "")
        {
            var newCounter = new CustomCounter(counterName, helpText, counterType);

            switch (counterType)
            {
                case PerformanceCounterType.AverageTimer32:
                    {
                        newCounter.BaseCounterType = PerformanceCounterType.AverageBase;
                        break;
                    }
            }

            this.Counters[counterName] = newCounter;
            return this;
        }

        public void CreateCounters()
        {
            CounterCreationDataCollection CounterDatas = new System.Diagnostics.CounterCreationDataCollection();

            foreach (CustomCounter c in this.Counters.Values)
            {
                CounterCreationData cdCounter = new System.Diagnostics.CounterCreationData();

                cdCounter.CounterName = c.Name;
                cdCounter.CounterHelp = c.HelpText;
                cdCounter.CounterType = c.CounterType;

                CounterDatas.Add(cdCounter);

                // Add base references
                if (c.HasBaseCounter)
                {
                    CounterCreationData cdCounterBase = new System.Diagnostics.CounterCreationData();

                    cdCounterBase.CounterName = c.BaseCounterName;
                    cdCounterBase.CounterHelp = c.BaseCounterName;
                    cdCounterBase.CounterType = c.BaseCounterType;

                    CounterDatas.Add(cdCounterBase);
                }
            }

            if (!PerformanceCounterCategory.Exists(this.categoryName))
            {
                Console.WriteLine("Creating performance counter category {0}:{1}", categoryName, categoryName);

                // Create the category and pass the collection to it.
                PerformanceCounterCategory.Create(categoryName,
                                                    categoryName,
                                                    PerformanceCounterCategoryType.SingleInstance,
                                                    CounterDatas);
            }
        }

        public static void ResetCounters(string categoryName = "")
        {
            List<PerfCounters> categoryList = null;

            if (!string.IsNullOrEmpty(categoryName))
            {
                categoryList = PerfCounters.categoryList.Values.Where(cl => cl.categoryName == categoryName).ToList();
            }

            else
            {
                // reset all
                categoryList = categoryList = PerfCounters.categoryList.Values.ToList();
            }


            foreach (var category in categoryList)
            {
                foreach (var counter in category.Counters.Values)
                {
                    Console.WriteLine(@"Resetting performance counter {0}\{1}", category, counter.Name);

                    var counterReference = PerfCounters.GetCounter(category.categoryName, counter.Name);
                    
                    if ( counterReference != null)
                    {
                        if (counterReference.BaseCounterReference != null)
                        {
                            counterReference.BaseCounterReference.RawValue = 0;
                        }

                        if (counterReference.CounterReference != null)
                        {
                            counterReference.CounterReference.RawValue = 0;
                        }
                    }
                }
            }
        }

        private void DeleteCategory()
        {
            if (PerformanceCounterCategory.Exists(this.categoryName))
            {
                Console.WriteLine("Deleting performance counter category {0}", categoryName);
                PerformanceCounterCategory.Delete(categoryName);
            }
        }

        /// <summary>
        /// Deletes the all performance counters and the category.
        /// </summary>
        public static void DeleteCounters()
        {
            foreach (var categoryName in PerfCounters.categoryList.Keys)
            {
                PerfCounters.categoryList[categoryName].DeleteCategory();
            }

            PerfCounters.categoryList.Clear();
        }

        /// <summary>
        /// Increments the specified counter by 1.
        /// </summary>
        public static void IncrementCounter(string categoryName, string counterName, long number = 1, long incrementBaseBy = 1) 
        {
            var cc = GetCounter(categoryName, counterName);

            switch (cc.CounterType)
            {
                case PerformanceCounterType.AverageTimer32:
                    {
                        cc.BaseCounterReference.IncrementBy(incrementBaseBy);
                        break;
                    }

                //case PerformanceCounterType.TotalOperations:
                  //  {
                    //    GetCounter(GenericCounters.OperationsPerSecond).IncrementBy(number);
                    //    break;
                    //}
            }

            cc.CounterReference.IncrementBy(number);
        }

        private CustomCounter GetCounter(string counterName, bool readOnly = false)
        {
            if (Counters.ContainsKey(counterName))
            {
                var cc = this.Counters[counterName];

                if (cc.CounterReference == null)
                {
                    lock (syncLock)
                    {
                        if (cc.CounterReference == null)
                        {
                            cc.CounterReference = new PerformanceCounter(categoryName, counterName, readOnly);
                        }

                        if (cc.HasBaseCounter &&
                            cc.BaseCounterReference == null)
                        {
                            cc.BaseCounterReference = new PerformanceCounter(categoryName, cc.BaseCounterName, readOnly);
                        }
                    }
                }

                return cc;
            }

            else
            {
                throw new ArgumentException("Counter not initialized!", counterName);
            }
        }

        private static CustomCounter GetCounter(string categoryName, string counterName, bool readOnly = false)
        {
            if (!categoryList.ContainsKey(categoryName))
            {
                throw new ArgumentException("Category not initialized!", categoryName);
            }

            else
            {
                return categoryList[categoryName].GetCounter(counterName, readOnly);
            }
        }
    }
}
