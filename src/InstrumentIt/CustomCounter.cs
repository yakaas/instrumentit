﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstrumentIt
{
    public sealed class CustomCounter
    {
        internal PerformanceCounterType CounterType;
        internal string Name = string.Empty;
        internal string HelpText = string.Empty;
        internal PerformanceCounter CounterReference = null;

        internal  PerformanceCounterType BaseCounterType;
        internal PerformanceCounter BaseCounterReference = null;



        public CustomCounter(string counterName)
            : this(counterName, counterName, PerformanceCounterType.NumberOfItems64)
        {
            
        }

        public CustomCounter(string counterName, PerformanceCounterType counterType)
            : this(counterName, counterName, counterType)
        {
        }

        public CustomCounter ( string counterName, string counterHelpText, PerformanceCounterType counterType)
        {
            this.Name = counterName;
            this.HelpText = counterHelpText;
            this.CounterType = counterType;
        }

        internal bool HasBaseCounter
        {
            get
            {
                switch (this.CounterType)
                {
                    case PerformanceCounterType.AverageTimer32:
                        {
                            return true;
                        }
                }

                return false;
            }
        }

        internal string BaseCounterName
        {
            get
            {
                return string.Format("{0}_Base", this.Name);

            }
        }
    }
}
