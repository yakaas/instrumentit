﻿
//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace InstrumentIt
//{
//    internal class PerfTmp
//    {
//        public static PerformanceCounter _TotalOperations = new PerformanceCounter();
//        public static PerformanceCounter _AverageDurationBase = new PerformanceCounter();
//        public static PerformanceCounter _AverageDuration = new PerformanceCounter();
//        public static PerformanceCounter _OperationsPerSecond = new PerformanceCounter();

//        public static string CategoryName = "__PoopManCounters__";
//        public static void Init()
//        {
//            if (!PerformanceCounterCategory.Exists(CategoryName))
//            {
//                CounterCreationDataCollection counters = new CounterCreationDataCollection();

//                // 1. counter for counting totals: PerformanceCounterType.NumberOfItems32
//                CounterCreationData totalOps = new CounterCreationData();
//                totalOps.CounterName = "# operations executed";
//                totalOps.CounterHelp = "Total number of operations executed";
//                totalOps.CounterType = PerformanceCounterType.NumberOfItems32;
//                counters.Add(totalOps);

//                // 2. counter for counting operations per second:
//                //        PerformanceCounterType.RateOfCountsPerSecond32
//                CounterCreationData opsPerSecond = new CounterCreationData();
//                opsPerSecond.CounterName = "# operations / sec";
//                opsPerSecond.CounterHelp = "Number of operations executed per second";
//                opsPerSecond.CounterType = PerformanceCounterType.RateOfCountsPerSecond32;
//                counters.Add(opsPerSecond);

//                // 3. counter for counting average time per operation:
//                //                 PerformanceCounterType.AverageTimer32
//                CounterCreationData avgDuration = new CounterCreationData();
//                avgDuration.CounterName = "average time per operation";
//                avgDuration.CounterHelp = "Average duration per operation execution";
//                avgDuration.CounterType = PerformanceCounterType.AverageTimer32;
//                counters.Add(avgDuration);

//                // 4. base counter for counting average time
//                //         per operation: PerformanceCounterType.AverageBase
//                CounterCreationData avgDurationBase = new CounterCreationData();
//                avgDurationBase.CounterName = "average time per operation base";
//                avgDurationBase.CounterHelp = "Average duration per operation execution base";
//                avgDurationBase.CounterType = PerformanceCounterType.AverageBase;
//                counters.Add(avgDurationBase);


//                // create new category with the counters above
//                PerformanceCounterCategory.Create(CategoryName, "Sample category for Codeproject", counters);

                
//            }

//            // create counters to work with
//            _TotalOperations.CategoryName = CategoryName;
//            _TotalOperations.CounterName = "# operations executed";
//            _TotalOperations.MachineName = ".";
//            _TotalOperations.ReadOnly = false;
//            _TotalOperations.RawValue = 0;

//            _OperationsPerSecond.CategoryName = CategoryName;
//            _OperationsPerSecond.CounterName = "# operations / sec";
//            _OperationsPerSecond.MachineName = ".";
//            _OperationsPerSecond.ReadOnly = false;
//            _OperationsPerSecond.RawValue = 0;

//            _AverageDuration.CategoryName = CategoryName;
//            _AverageDuration.CounterName = "average time per operation";
//            _AverageDuration.MachineName = ".";
//            _AverageDuration.ReadOnly = false;
//            _AverageDuration.RawValue = 0;

//            _AverageDurationBase.CategoryName = CategoryName;
//            _AverageDurationBase.CounterName = "average time per operation base";
//            _AverageDurationBase.MachineName = ".";
//            _AverageDurationBase.ReadOnly = false;
//            _AverageDurationBase.RawValue = 0;
//        }
//    }
//}
