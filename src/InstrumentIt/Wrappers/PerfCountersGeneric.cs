﻿
using Autofac.Registrars;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;


namespace InstrumentIt
{
    public abstract class PerfCounters<T> :  IFluentInterface 
                                    where T : struct, IConvertible 
    {
        private PerfCounters perfCountersReference = null;

        static PerfCounters()
        {
            if (!typeof(T).IsEnum) throw new ArgumentException("T must be an enumerated type");
        }

        public abstract PerfCounters<T> CreateInstance(string categorySuffix, bool recreateCategory);

        
        public virtual PerfCounters<T> Init(string categorySuffix = "default", bool recreateCategory = false)
        {
            var categoryName = GetCategoryName(categorySuffix);

            var counterRefeence = this; 
            
            counterRefeence.perfCountersReference = PerfCounters.Initialize(recreateCategory)
                                                                .WithCategory(categoryName);
            return counterRefeence; 
        }

        public virtual PerfCounters<T> OnMachine(string machineName)
        {
            VerifyInitialization();

            perfCountersReference.OnMachine(machineName);
            return this;
        }

        private void VerifyInitialization()
        {
            if (perfCountersReference == null)
            {
                throw new NullReferenceException("Not initialized! Call initizlize before calling this method.");
            }
        }

        private static string GetCategoryName(string categorySuffix)
        {
            var categoryName = typeof(T).Name;

            if (categorySuffix != "default")
            {
                categoryName = string.Format("{0}.{1}", categoryName, categorySuffix);
            }

            return categoryName; 
        }

        public PerfCounters<T> AndItemCounter(T counterName)
        {
            VerifyInitialization();

            perfCountersReference.AndItemCounter(Parse(counterName).GetDescription());
            return this;
        }

        public PerfCounters<T> AndRateCounter(T counterName)
        {
            VerifyInitialization();

            perfCountersReference.AndRateCounter(Parse(counterName).GetDescription());
            return this;

        }

        public PerfCounters<T> AndAverageTimerCounter(T counterName)
        {
            VerifyInitialization();

            perfCountersReference.AndAverageTimerCounter(Parse(counterName).GetDescription());
            return this;
        }

        #region Static Methods

        public void CreateCounters()
        {
            VerifyInitialization();

            perfCountersReference.CreateCounters();
        }

        public static void ResetCounters()
        {
            PerfCounters.ResetCounters();
        }

        /// <summary>
        /// Deletes the all performance counters and the category.
        /// </summary>
        public void DeleteCounters()
        {
            PerfCounters.DeleteCounters();
            this.ClearHandles();
        }

        protected abstract void ClearHandles();

        /// <summary>
        /// Increments the specified counter by 1.
        /// </summary>
        public void IncrementCounter(T counter, long number = 1) 
        {
            PerfCounters.IncrementCounter(this.perfCountersReference.CategoryName, Parse(counter).GetDescription(), number);
        }

        private static Enum Parse(T value)
        {
            return (Enum)(Enum.Parse(typeof(T), value.ToString()));
        }

        #endregion
    }
}
