﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstrumentIt
{
    public enum InstrumentIt
    {
        [Description("# operations executed")]
        TotalOperations,

        [Description("# operations / sec")]
        OperationsPerSecond,

        [Description("# total errors")]
        TotalErrors,

        [Description("# total retries")]
        TotalRetries,

        [Description("average time per operation")]
        AverageTimePerOperation,

        //[Description("average time per operation base")]
        //AverageTimePerOperationBase
    };


    public class PerfCountersDefault : PerfCounters<InstrumentIt>, IDisposable
    {
        private long startTicks = 0;
        static object sync = new object();

        static IDictionary<string, PerfCountersDefault> referenceList = new Dictionary<string, PerfCountersDefault>();


        private PerfCountersDefault(string categorySuffix = "default", bool recreateCategory = false)
        {
            this.startTicks = DateTime.Now.Ticks;
        }

        public static PerfCountersDefault GetInstance(string categorySuffix = "default")
        {
            if (!referenceList.Keys.Contains(categorySuffix))
            {
                lock (sync)
                {
                    var reference = new PerfCountersDefault();
                    referenceList[categorySuffix] = reference;
                }
            }

            // Reset start time before returning the instance reference,
            referenceList[categorySuffix].startTicks = DateTime.Now.Ticks;

            return referenceList[categorySuffix];
        }


        public static PerfCountersDefault Initialize (string categorySuffix = "default", bool recreateCategory = false)
        {
            if (referenceList.Keys.Contains(categorySuffix))
            {
                throw new ArgumentException(string.Format("Category with the suffix {0}, already initialized!", categorySuffix));
            }

            else
            {
                lock (sync)
                {
                    var reference = PerfCountersDefault.CreateInstanceLocal(categorySuffix, recreateCategory);

                    reference.AndItemCounter(InstrumentIt.TotalOperations)
                             .AndItemCounter(InstrumentIt.TotalErrors)
                             .AndItemCounter(InstrumentIt.TotalRetries)
                             .AndRateCounter(InstrumentIt.OperationsPerSecond)
                             .AndAverageTimerCounter(InstrumentIt.AverageTimePerOperation)
                             ;

                    referenceList[categorySuffix] = reference;
                }

            }

            referenceList[categorySuffix].startTicks = DateTime.Now.Ticks;
            return referenceList[categorySuffix];
        }

        public void NewError()
        {
            base.IncrementCounter(InstrumentIt.TotalErrors);
        }

        public void NewRetry()
        {
            base.IncrementCounter(InstrumentIt.TotalRetries);
        }

        public void Dispose()
        {
            long stopTicks = DateTime.Now.Ticks;

            base.IncrementCounter(InstrumentIt.TotalOperations);
            base.IncrementCounter(InstrumentIt.OperationsPerSecond);
            base.IncrementCounter(InstrumentIt.AverageTimePerOperation, stopTicks-startTicks);
            //PerfCountersDefault.IncrementCounter(InstrumentIt.AverageTimePerOperationBase);
        }

        public override PerfCounters<InstrumentIt> CreateInstance(string categorySuffix, bool recreateCategory)
        {
            return PerfCountersDefault.CreateInstanceLocal(categorySuffix, recreateCategory);
        }
        private static PerfCountersDefault CreateInstanceLocal(string categorySuffix, bool recreateCategory)
        {
            var reference = new PerfCountersDefault();
            reference.Init(categorySuffix, recreateCategory);
            return reference ;
        }

        protected override void ClearHandles()
        {
            PerfCountersDefault.referenceList.Clear();
        }
    }
}
