﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstrumentIt
{
    public partial class PerfCounters
    {
        public PerfCounters AndItemCounter(string counterName, string helpText = "")
        {
            return AndCounter(counterName, PerformanceCounterType.NumberOfItems64, helpText);
        }

        public PerfCounters AndRateCounter(string counterName, string helpText = "")
        {
            return AndCounter(counterName, PerformanceCounterType.RateOfCountsPerSecond32, helpText);
        }

        public PerfCounters AndAverageTimerCounter(string counterName, string helpText = "")
        {
            return this.AndCounter(counterName, PerformanceCounterType.AverageTimer32);
        }

        ////public PerfCounters<T> AndAverageCounter(T counterName, T baseCounter)
        ////{
        ////    this.AndCounter(counterName, PerformanceCounterType.AverageCount64);
        ////    return this.AndCounter(baseCounter, PerformanceCounterType.AverageBase);
        ////}

    }
}
