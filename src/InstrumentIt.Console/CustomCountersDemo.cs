﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstrumentIt.Console
{
    public class CustomCountersDemo
    {
        public static void Test()
        {
            PerfCountersDefault.Initialize().OnMachine("PANDORA").DeleteCounters();
            PerfCountersDefault.Initialize("Resume-Get").OnMachine("PANDORA").DeleteCounters();
            PerfCountersDefault.Initialize("DiskActivity").OnMachine("PANDORA").DeleteCounters();
            PerfCountersDefault.Initialize("Resume-Post").OnMachine("PANDORA").DeleteCounters();


            PerfCountersDefault.Initialize().OnMachine("PANDORA").CreateCounters();
            PerfCountersDefault.Initialize("Resume-Get").OnMachine("PANDORA").CreateCounters();
            PerfCountersDefault.Initialize("DiskActivity").OnMachine("PANDORA").CreateCounters();
            PerfCountersDefault.Initialize("Resume-Post").OnMachine("PANDORA").CreateCounters();

            //PerfCounters.ResetCounters();

            PerfCountersDefault.ResetCounters();


            int retry = 0;

            for (int i = 0; i < 1000; i++)
            {
                int k = new Random(i).Next(1000);

                System.Threading.ThreadPool.QueueUserWorkItem((args1) =>
                {
                    using (var p = PerfCountersDefault.GetInstance())
                    {
                        while (++retry < 2)
                        {
                            try
                            {
                                // do work
                                System.Console.WriteLine("cycle {0} seed {1}", i, k);
                                System.Threading.Thread.Sleep(k);


                                // Generate error
                                if (k < 10)
                                {
                                    System.Console.WriteLine("cycle {0} seed {1}: Generating an error", i, k);
                                    throw new ApplicationException();
                                }

                                // API Call - GET
                                using (var apiGet = PerfCountersDefault.GetInstance("Resume-Get"))
                                {
                                    var a1 = new Random(1000).Next(1000);
                                    System.Console.WriteLine(a1);
                                    System.Threading.Thread.Sleep(a1);
                                }


                                // API Call POST
                                using (var apiGet = PerfCountersDefault.GetInstance("Resume-Post"))
                                {
                                    var a1 = new Random(3000).Next(3000);
                                    System.Console.WriteLine(a1);
                                    System.Threading.Thread.Sleep(a1);
                                }

                                // Disk operation
                                using (var apiGet = PerfCountersDefault.GetInstance("DiskActivity"))
                                {
                                    var a1 = new Random(5000).Next(5000);
                                    System.Console.WriteLine(a1);
                                    System.Threading.Thread.Sleep(a1);
                                }
                            }

                            catch
                            {
                                p.NewError();
                                p.NewRetry();

                                retry++;
                            }
                        }

                        retry = 0; 
                    }
                });

                //System.Threading.Thread.Sleep(100);
            }


        }
    }
}
