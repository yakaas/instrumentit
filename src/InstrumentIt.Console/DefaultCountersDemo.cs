﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstrumentIt.Console
{
    public class DefaultCountersDemo
    {
        public static void RunTest()
        {
            PerfCountersDefault.Initialize().DeleteCounters();
            //PerfCountersDefault.DeleteCounters();

            PerfCountersDefault.Initialize().CreateCounters();
            PerfCountersDefault.ResetCounters();



            int retry = 0;

            for (int i = 0; i < 1000; i++)
            {
                int k = new Random(i).Next(1000);

                System.Threading.ThreadPool.QueueUserWorkItem((args1) =>
                {
                    using (var p = PerfCountersDefault.GetInstance())
                    {
                        while (++retry < 2)
                        {
                            try
                            {
                                // do work
                                System.Console.WriteLine("cycle {0} seed {1}", i, k);
                                System.Threading.Thread.Sleep(k);
                            }

                            catch
                            {
                                //PerfCountersDefault.GetInstance().
                            }
                        }
                    }
                });

                System.Threading.Thread.Sleep(100);
            }
        }
    }
}
