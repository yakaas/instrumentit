﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstrumentIt.Console
{
    class Program
    {
        //    public enum GenericCounters
        //    {
        //        [Description("# operations executed")]
        //        TotalOperations,

        //        [Description("# operations / sec")]
        //        OperationsPerSecond,

        //        [Description("average time per operation")]
        //        AverageTimePerOperation,

        //        [Description("average time per operation base")]
        //        AverageTimePerOperationBase
        //    };
        static void Main(string[] args)
        {
            while (true)
            {
                System.Console.Clear();
                System.Console.WriteLine("1. Custom counter test");


                var keyInfo = System.Console.ReadKey();

                switch (keyInfo.Key)
                {
                    case ConsoleKey.D1:
                        {
                            CustomCountersDemo.Test();
                            break;
                        }
                }

                System.Console.WriteLine("Demo completed, press any key to continue...");
                System.Console.ReadLine();
            }


            //PerfCounters.DeleteCounters();

            //PerfCounters.Initialize()
            //            .WithCategory("TestCategory")
            //            .AndItemCounter("TotalOperations", "# operations executed")
            //            .AndRateCounter("OperationsPerSecond", "# operations / sec")
            //            //.AndAverageTimerCounter()
            ////            .AndAverageTimerCounter(GenericCounters.AverageTimePerOperation, GenericCounters.AverageTimePerOperationBase)
            //            .CreateCounters();

            //PerfCounters.ResetCounters();


             //   DefaultCountersDemo.RunTest();

            //PerfCountersDefault.IncrementCounter(InstrumentIt.TotalOperations);

            //System.Console.ReadLine();
        }
    }
}
